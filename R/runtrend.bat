rem
rem   To configure this runtrend.bat for your environment, do the following.
rem
rem   Look first for the path to Rscript.exe: in cmd window, go to C:\Users, and
rem   type:   where /r . Rscript.exe
rem
rem   Then copy one of the results, and paste it between the quotes in the command
rem   line at the end of this file.
rem
rem   Now you can run runtrend.bat with the same arguments as used for runtrend.R
rem   (see header of runtrend.R).
rem
rem   If you want to call it from folders not containing runtrend.R and climatrend.R,
rem   then replace runtrend.R in the command below by its full path,
rem   and modify sourcepath in the code of runtrend.R accordingly.
rem
rem   version: 17-Mar-2021
rem
rem   author: Cees de Valk (cees.de.valk@knmi.nl)
rem
rem   reference: KNMI Technical report TR-389 (see http://bibliotheek.knmi.nl/knmipubTR/TR389.pdf)
rem
"C:\Users\All Users\App-V\2C43EEE5-D738-431B-80BC-A8EC97345439\BE788D1A-CBF7-4AD7-8FCD-C84F21CE2D15\Root\R-4.0.2\bin\Rscript.exe" runtrend.R %1 %2 %3 %4 %5 %6 %7

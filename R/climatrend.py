# name:  climatrend
#
# description: Fit a trendline to an annually sampled time-series by local linear regression (LOESS)
#
# usage: Value = climatrend(t, y, p, t1, t2, ybounds, drawplot, draw30)
#
#    t: years, increasing by 1 (double(n))
#    y: annual values; missing values as blanks are allowed near beginning and end (double(n))
#    p: (optional) confidence level for error bounds (default: 0.95)) (double)
#    t1: (optional) first year for which trendline value is compared in test (double)
#    t2: (optional) second year (see t1)
#    ybounds: (optional) lower/upper bound on value range of y (default: c(-Inf, Inf)) (double(2))
#    drawplot: (optional): FALSE/TRUE or label on y-axis (default: FALSE) (logical or character)
#    draw30: (optional): add 30-year moving averages to plot (default: FALSE) (logical).
#
# return: A dictionary with keys
#
#    t {years}
#    trend {trendline in y for years in t}
#    p {confidence level}
#    trendubound {lower confidence limit}
#    trendlbound {upper confidence limit}
#    averaget {central value of t in a 30-year interval}
#    averages {30-year average of y}
#    t1 {first year for which trendline value is compared in test}
#    t2 {second year for which trendline value is compared in test}
#    pvalue {of test of no longterm change}
#    ybounds {bounds on value range of y}
#
# details:
#
#   Calls R-function climatrend.R for computation. Modify the path to the R-code at the  
#   beginning of the code below and in R.
#
#   The trendline can be regarded as an approximation of a 30-year average, which has a smooth
#   appearance and is extended toward the beginning and end of the time-series.
#
#   It is based on linear local regression, computed using the R-function loess.
#   It uses a bicubic weight function over a 42-year window. In the central part
#   of the time-series, the variance of the trendline estimate is approximately equal to
#   the variance of a 30-year average.
#
#   To test the proposition of no long-term change between the years t1 and t2, these years
#   need to be supplied. The result is the p-value: the probability (under the proposition)
#   that the estimated trendline values in t2 and t1 differ more than observed.
#
#   version: 18-Mar-2021
#
# references: KNMI Technical report TR-389 (see http://bibliotheek.knmi.nl/knmipubTR/TR389.pdf)
#
# author: Cees de Valk \email{cees.de.valkknmi.nl}
#

def climatrend(t, y, p= 0.95, t1= None, t2= None, ybounds= None, drawplot= False, draw30= False):
   
   # Modify path to R source files if needed. Do the same in runtrend.R!
   # sourcepath= "/Users/ceesdevalk/Documents/KNMI/Projects/trend_knmi/R"  # modify if needed!
   
   import platform
   import os
   import pandas as pd
   import numpy as np
   import linecache
   import time
   import subprocess
   import random

   sourcepath= os.path.dirname(os.path.abspath(__file__))  # path of current file

   # detect OS

   OS= platform.system()


   # create panda dataframe

   df= pd.DataFrame({'t': t,'y': y}, columns = ['t', 'y'])


   # create header line

   header= ""
   header= header + " p=" + str(p)
   if t1 is not None:
      header= header + " t1=" + str(t1) 
   else: 
      t1= float('NaN')
   if t2 is not None:
      header= header + " t2=" + str(t2) 
   else: 
      t1= float('NaN')
   if ybounds is not None:
      header= header + " lbound=" + str(min(ybounds)) 
      header= header + " ubound=" + str(max(ybounds)) 
   if drawplot:
      if isinstance(drawplot, str):
         header= header + " drawplot='" + drawplot +"'"
      else:
         header= header + " drawplot=TRUE"
   if draw30:
      header= header + " draw30=TRUE" 
   header= header.strip()


   # write input file
   filename = "data-{}.csv".format(random.randint(0, 1e8))
   infile= os.path.abspath(filename)
   a= open(infile, "w")
   a.write(header + "\n")
   df.to_csv(a, index= False, header= 1)
   a.close()


   # call Rscript runtrend.R ....  or Rscript runtrend.bat .... on the command line 

   if OS == "Windows":
      comman= os.path.join(sourcepath, 'runtrend.bat') + ' ' + infile
      os.system(comman)
   else:
      comman= 'Rscript ' + os.path.join(sourcepath, 'runtrend.R') + ' ' + infile 
      # os.system(comman)
      subprocess.call(['Rscript', 'runtrend.R', infile], cwd=sourcepath)


   # read output file

   outfile = os.path.abspath('trend_' + filename)
   if not os.path.exists(outfile):
      raise Error
   Data= pd.read_csv(outfile, skiprows= 3)
   Data = Data.apply(pd.to_numeric, errors='coerce')
   a= open(outfile, "r")
   line= linecache.getline(outfile, 2)
   if len(line)> 2:
      pvalue= float(line)
   else:
      pvalue= float('NaN')
   a.close()


   # remove input and output files

   if OS == "Windows":
      comman= 'del ' + infile 
      os.system(comman)
      comman= 'del ' + outfile 
      os.system(comman)
   else:
      comman= 'rm ' + infile 
      os.system(comman)
      comman= 'rm ' + outfile 
      os.system(comman)


   # return results in dictionary

   t= Data["t"].to_numpy()
   trend= Data["trend"].to_numpy()
   trendlbound= Data["trendlowerbound"].to_numpy()
   trendubound= Data["trendupperbound"].to_numpy()
   av30= Data["30-yr average"].to_numpy()
   id= ~pd.isnull(av30)
   averagey= av30[id]
   averaget= (Data["t"][id]-14.5).to_numpy()
   
   Value = dict();   
   Value['t']= t
   Value['trend']= trend
   Value['p']= p
   Value['trendubound']= trendubound
   Value['trendlbound']= trendlbound
   Value['averaget']= averaget
   Value['averagey']= averagey
   Value['t1']= t1
   Value['t2']= t2
   Value['pvalue']= pvalue
   Value['ybounds']= ybounds
   
   return Value
 


